#!/bin/bash

name=$1
version=$2

rm -rf ./dist
mkdir -p dist
git archive --worktree-attributes --format tar --prefix ${name}-${version}/ feature/authentication-graphql | xz -9ve > dist/${name}-${version}.tar.xz
gpg2 --output dist/${name}-${version}.tar.xz.asc --digest-algo SHA512 --armor --detach-sig dist/${name}-${version}.tar.xz
