#!/bin/bash -eu

if [ -d '/run/systemd/system' ]; then
  SERVICE_TEST='systemctl is-active --quiet'
else
  SERVICE_TEST='pgrep -x'
fi

eval $SERVICE_TEST "$1" && echo "$1 service running" || (echo "$1 service not running" && exit 1)
