import editProfileGql from '~/apollo/mutations/editProfile'
import changePasswordGql from '~/apollo/mutations/changePassword'
import deleteAccountGql from '~/apollo/mutations/deleteAccount'

export const state = () => ({
  data: {},
  isProfileDrawerOpen: false
})

export const mutations = {
  toggleProfileDrawer(state) {
    state.isProfileDrawerOpen = !state.isProfileDrawerOpen
  },
  set(state, data) {
    state.data = data
  }
}

export const getters = {
  isProfileDrawerOpen: (state, getters, rootState) => {
    return rootState.auth.isAuthenticated ? state.isProfileDrawerOpen : false
  },
  getImageUrl(state) {
    return state.data && state.data.image ? '/media/' + state.data.image : ''
  },
  get: (state, getters) => {
    return state.data
  }
}

export const actions = {
  async setRemote({ commit }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: editProfileGql,
          variables: input
        })
        .then(({ data }) => data.profile)
      commit('profile/set', res, { root: true })
    } catch (e) {
      console.log(e)
    }
  },
  async changePassword({ commit }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: changePasswordGql,
          variables: input
        })
        .then(({ data }) => data.changePassword)
      return res
    } catch (e) {
      console.error(e)
    }
  },
  async deleteAccount({ commit }, email, password) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: deleteAccountGql,
          variables: {
            email: email,
            password: password
          }
        })
        .then(({ data }) => data)
      if (res.success) {
        commit('auth/logout', { root: true })
        this.app.$apolloHelpers.onLogout()
      }
      return res
    } catch (e) {
      console.error(e)
    }
  }
}
