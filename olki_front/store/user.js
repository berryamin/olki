export const state = () => ({
  data: {}
})

export const mutations = {
  set(state, data) {
    state.data = data
  }
}

export const getters = {
  get: (state, getters) => {
    return state.data
  },
  isAdministrator: (state, getters) => {
    return getters.get ? getters.get.isAdministrator : false
  },
  isModerator: (state, getters) => {
    return getters.get ? getters.get.isModerator : false
  },
  isSetup: state => {
    return state.data ? state.data.isSetup : false
  },
  displayName: state => {
    if (state.data && state.data.email) {
      return state.data.profile.name !== ''
        ? state.data.profile.name
        : state.data.actor
        ? state.data.actor.username
        : state.data.email.substring(0, 2).toUpperCase()
    } else {
      return ''
    }
  }
}
