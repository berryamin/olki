export const state = () => ({
  data: {}
})

export const mutations = {
  set(state, data) {
    state.data = data
  }
}

export const getters = {
  openRegistrations(state) {
    return state.data.openRegistrations
  },
  getTerms(state) {
    return state.data.metadata ? state.data.metadata.nodeTerms : ''
  },
  getRules(state) {
    return state.data.metadata ? state.data.metadata.nodeRules : ''
  }
}

export const actions = {
  async syncNodeinfo({ commit }) {
    try {
      const res = await this.$axios.$get('.well-known/nodeinfo/2.1')
      commit('set', res)
    } catch (e) {
      console.log(e)
    }
  }
}
