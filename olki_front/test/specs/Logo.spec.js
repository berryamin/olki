import { mount } from '@vue/test-utils'
import test from 'ava'
import Heading from '../../components/Heading.vue'

test('is a Vue instance', t => {
  const wrapper = mount(Heading)
  t.is(wrapper.isVueInstance(), true)
})
