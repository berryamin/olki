const pkg = require('./package')
require('dotenv').config({
  path: '../.env'
})

module.exports = {
  mode: 'spa',
  generate: {
    fallback: true
  },

  /*
   ** Headers of the page
   */
  head: {
    title: 'OLKi',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    noscript: [{ innerHTML: 'This website requires JavaScript.' }],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#205e3b' },

  /*
   ** Global CSS
   */
  css: [
    { src: '~plugins/themes/olki-default.scss', lang: 'scss' }
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~plugins/persistedstate', ssr: false },
    { src: '~plugins/init', ssr: false },
    '~plugins/themes/olki-default.js',
    '~plugins/vue2-filters',
    { src: '~plugins/vee-validate', ssr: false },
    { src: '~plugins/vue-observe-visibility', ssr: false }
  ],

  /*
   ** Router
   */
  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'home',
        path: '/',
        component: resolve(__dirname, 'pages/index.vue')
      }),
      routes.push({
        path: '/*',
        component: resolve(__dirname, 'layouts/error.vue')
      })
    }
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/apollo',
    'nuxt-compress',
    ['@nuxtjs/toast', {
      position: 'bottom-center',
      duration: 3000
    }],
    ['nuxt-i18n', {
      defaultLocale: 'en',
      noPrefixDefaultLocale: true,
      lazy: true,
      langDir: 'lang/',
      locales: [
        {
          code: 'en',
          name: 'English',
          iso: 'en-US',
          file: 'en-US.js'
        },
        {
          code: 'es',
          name: 'Español',
          iso: 'es-ES',
          file: 'es-ES.js'
        },
        {
          code: 'fr',
          name: 'Français',
          iso: 'fr-FR',
          file: 'fr-FR.js'
        },
        {
          code: 'de',
          name: 'Deutsch',
          iso: 'de-DE',
          file: 'de-DE.js'
        }
      ],
      detectBrowserLanguage: {
        useCookie: true,
        alwaysRedirect: true,
        fallbackLocale: 'en'
      }
    }],
    ['nuxt-polyfill', {
      features: [
        {
          require: 'focus-visible' // detecting :focus-visible selector support is not easy, so we include it for everyone
        },
        {
          require: 'intersection-observer',
          detect: () => 'IntersectionObserver' in window // the polyfill will not be loaded, parsed and executed if it's not necessary
        }
      ]
    }]
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    browserBaseURL: '/'
  },

  /*
   ** Compression options to gzip/brotli assets
   *
   * This makes the 'generate' command pre-compile gzip and brotli versions of the frontend,
   * so that the server can disable compressing and just do redirections. No more heavy-lifting
   * for the server!
   */
  "nuxt-compress": {
    gzip: {
      cache: true
    },
    brotli: {
      threshold: 10240
    }
  },

  // Give apollo module options
  apollo: {
    tokenName: 'tohka',
    authenticationType: 'JWT',
    clientConfigs: {
      default: {
        httpEndpoint: 'http://localhost:5000/graphql', // process.env.INSTANCE_HOST + '/graphql',
        httpLinkOptions: {
          credentials: 'same-origin'
        }
      },
      noAuth: '~/apollo/noAuthClient.js'
    }
  },

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }
  },

  /*
   ** Service Worker configuration
   */
  cacheAssets: false, // for /*
  offline: false, // for /_nuxt/*

  /*
   ** Build configuration
   */
  build: {
    transpile: [/^zutre/],

    /*
     ** PostCSS config
     */
    postcss: {
      'postcss-preset-env': {
        browsers: [
          '> .5%',
          'last 2 versions',
          'Firefox ESR',
          'not ie > 0',
          'not ie_mob > 0',
          'not op_mini all',
          'not dead',
          'not < .1%'
        ]
      }
    },

    /*
     ** Webpack config extension
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      } else {
        config.output.publicPath = process.env.INSTANCE_HOST
      }
    }
  }
}
