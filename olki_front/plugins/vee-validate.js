import Vue from 'vue'
import {
  Validator,
  install as VeeValidate
} from 'vee-validate/dist/vee-validate.minimal.esm.js'
import {
  required,
  min,
  max,
  dimensions,
  alpha,
  image,
  ext,
  is,
  email
} from 'vee-validate/dist/rules.esm.js'
import veeEn from 'vee-validate/dist/locale/en'

// Add the rules you need.
Validator.extend('required', required)
Validator.extend('alpha', alpha)
Validator.extend('dimensions', dimensions)
Validator.extend('image', image)
Validator.extend('email', email)
Validator.extend('ext', ext)
Validator.extend('min', min)
Validator.extend('max', max)
Validator.extend('is', is)

// Merge the messages.
Validator.localize('en', veeEn)

Vue.use(VeeValidate, {
  inject: false // augments performance by not injecting unless explicitly told to as follows
})

/* Injection snippet to add to components:
  $_veeValidate: {
    validator: 'new'
  },
*/
