export default async ({ store }) => {
  await store.dispatch('auth/init')
  await store.dispatch('instance/syncNodeinfo')

  setTimeout(() => store.dispatch('auth/verify')) // has to be put after init and sync

  setInterval(() => store.dispatch('auth/verify'), 300000) // 5 minutes
}
