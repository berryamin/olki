## Credits

This application uses Open Source components. You can find the source code of
their open source projects along with license information below. We acknowledge
and are grateful to these developers for their contributions to open source.

    Project: Funkwhale https://dev.funkwhale.audio/funkwhale/funkwhale
    License (AGPLv3.0 only) https://dev.funkwhale.audio/funkwhale/funkwhale/blob/0.17/LICENSE
    Usage: part of the federation and HTTP signature code

    Project: Pinafore https://github.com/nolanlawson/pinafore
    License (AGPLv3.0 only) https://github.com/nolanlawson/pinafore/blob/v0.16.0/LICENSE
    Usage: store for svelte components, utils, and global inspiration for the use of sveltejs/sapperjs

    Project: Inter UI https://rsms.me/inter/
    License (SIL Open Font License 1.1) https://github.com/rsms/inter/blob/v3.2/LICENSE.txt
    Usage: default font for the default frontend
