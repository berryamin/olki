"""olki URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from typing import List
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from revproxy.views import ProxyView
from graphql_jwt.decorators import jwt_cookie

from .apps.core import urls as core_urls
from .apps.core.graphql.upload.django import FileUploadGraphQLView

V1_PATTERNS: List[str] = [
]

urlpatterns: List[str] = [
    # In production, '/' is serving staticfiles via django-spa/whitenoise automagically.

    # API
    url('graphql', csrf_exempt(jwt_cookie(FileUploadGraphQLView.as_view(graphiql=True)))),
    url('api/v1/', include((V1_PATTERNS, 'v1'), namespace='v1')),

    # ActivityPub, WebFinger
    url('', include((core_urls.urlpatterns, 'core'), namespace='core'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG and settings.CLIENT_ENABLED:
    urlpatterns += [url(r'^(?P<path>.*)$', ProxyView.as_view(upstream=settings.CLIENT_URL))]
