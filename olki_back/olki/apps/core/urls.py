from django.urls import path

from .views import nodeinfo_well_known_view, nodeinfo1_view, nodeinfo2_view, nodeinfo21_view

app_name = 'core'
urlpatterns = [
    path('.well-known/nodeinfo', nodeinfo_well_known_view),
    path('.well-known/nodeinfo/1.0', nodeinfo1_view),
    path('.well-known/nodeinfo/2.0', nodeinfo2_view),
    path('.well-known/nodeinfo/2.1', nodeinfo21_view)
]
