import random
import string
from django.conf import settings

DEFAULT_CHAR_STRING = string.ascii_lowercase + string.digits


def generate_random_string(chars=DEFAULT_CHAR_STRING, size=6):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_nodeinfo_metadata():
    return {
        "staff_accounts": [],
        "taxonomy": {
            "postsName": "Corpus"
        },
        "nodeName": settings.SITE_NAME,
        "nodeDefaultClientRoute": "home",
        "nodeDescription": settings.INSTANCE_DESCRIPTION,
        "nodeTerms": settings.INSTANCE_TERMS,
        "nodeRules": settings.INSTANCE_RULES
    }
