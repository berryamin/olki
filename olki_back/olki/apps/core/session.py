import requests
from django.conf import settings
from olki import __version__


def get_user_agent():
    return "python-requests (olki/{}; +{})".format(
        __version__, settings.OLKI_URL
    )


def get_session():
    s = requests.Session()
    s.headers["User-Agent"] = get_user_agent()
    return s
