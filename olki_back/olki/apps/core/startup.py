from django.apps import AppConfig


class OlkiAppConfig(AppConfig):
    """
    Override this method in subclasses to run code when Django starts.

    This code will be run once at startup
    """
    name = 'olki'
    verbose_name = "OLKi"

    def ready(self):
        pass  # startup code here
