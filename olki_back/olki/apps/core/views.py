import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from django.http.response import JsonResponse

from olki import __version__
from .utils import generate_nodeinfo_metadata


def nodeinfo_well_known_view(request):
    """Generate a NodeInfo .well-known document.

    See spec: http://nodeinfo.diaspora.software

    :returns: dict
    """
    return JsonResponse({
        "links": [
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/1.0",
                "href": "{url}{path}".format(
                    url=settings.OLKI_URL, path='/.well-known/nodeinfo/1.0'
                )
            },
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                "href": "{url}{path}".format(
                    url=settings.OLKI_URL, path='/.well-known/nodeinfo/2.0'
                )
            },
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1",
                "href": "{url}{path}".format(
                    url=settings.OLKI_URL, path='/.well-known/nodeinfo/2.1'
                )
            }
        ]
    })


def nodeinfo1_view(request):
    """Generate a NodeInfo 1.0 document."""
    usage = {"users": {}}
    data = {
        "version": "1.0",
        "software": {
            "name": "olki",
            "version": __version__
        },
        "protocols": {"inbound": ["activitypub"], "outbound": ["activitypub"]},
        "services": {"inbound": [], "outbound": []},
        "openRegistrations": settings.INSTANCE_REGISTRATIONS_OPEN,
        "usage": usage,
        "metadata": generate_nodeinfo_metadata()
    }
    if not settings.INSTANCE_REDUCED_STATISTICS:
        data.update({"usage": {
            "users": {
                "total": get_user_model().objects.count(),
                "activeHalfyear": get_user_model().objects.filter(
                    last_login__gte=now() - datetime.timedelta(days=180)).count(),
                "activeMonth": get_user_model().objects.filter(
                    last_login__gte=now() - datetime.timedelta(days=30)).count(),
            },
            # "localPosts": #TODO,
            # "localComments": #TODO,
        }})
    return JsonResponse(data)


def nodeinfo2_view(request):
    """Generate a NodeInfo 2.0 document."""
    usage = {"users": {}}
    data = {
        "version": "2.0",
        "software": {
            "name": "olki",
            "version": __version__
        },
        "protocols": ["activitypub"],
        "services": {"inbound": [], "outbound": []},
        "openRegistrations": settings.INSTANCE_REGISTRATIONS_OPEN,
        "usage": usage,
        "metadata": generate_nodeinfo_metadata()
    }
    if not settings.INSTANCE_REDUCED_STATISTICS:
        data.update({"usage": {
            "users": {
                "total": get_user_model().objects.count(),
                "activeHalfyear": get_user_model().objects.filter(last_login__gte=now() - datetime.timedelta(days=180)).count(),
                "activeMonth": get_user_model().objects.filter(last_login__gte=now() - datetime.timedelta(days=30)).count(),
            },
            # "localPosts": #TODO,
            # "localComments": #TODO,
        }})
    return JsonResponse(data)


def nodeinfo21_view(request):
    """Generate a NodeInfo 2.1 document."""
    usage = {"users": {}}
    data = {
        "version": "2.1",
        "software": {
            "name": "olki",
            "version": __version__,
            "repository": "https://framagit.org/synalp/olki/olki",
            "homepage": "https://olki.loria.fr/platform/"
        },
        "protocols": ["activitypub"],
        "services": {"inbound": [], "outbound": []},
        "openRegistrations": settings.INSTANCE_REGISTRATIONS_OPEN,
        "usage": usage,
        "metadata": generate_nodeinfo_metadata()
    }
    if not settings.INSTANCE_REDUCED_STATISTICS:
        data.update({"usage": {
            "users": {
                "total": get_user_model().objects.count(),
                "activeHalfyear": get_user_model().objects.filter(last_login__gte=now() - datetime.timedelta(days=180)).count(),
                "activeMonth": get_user_model().objects.filter(last_login__gte=now() - datetime.timedelta(days=30)).count(),
            },
            # "localPosts": #TODO,
            # "localComments": #TODO,
        }})
    return JsonResponse(data)
