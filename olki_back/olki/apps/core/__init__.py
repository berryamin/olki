import os

"""
Prevent the config from running twice in the development environment
which uses another process to check for reload
"""

if os.environ.get('RUN_MAIN', None) != 'true':
    default_app_config = "olki.apps.core.startup.OlkiAppConfig"
