from dynamic_preferences.types import BooleanPreference, StringPreference, LongStringPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry

from olki.apps.core import preferences

general = Section('general')


@global_preferences_registry.register
class SiteTitle(
        preferences.DefaultFromSettingMixin,
        StringPreference
):
    section = general
    name = 'site_title'
    default = 'OLKi'
    required = False


@global_preferences_registry.register
class SiteDescription(
        preferences.DefaultFromSettingMixin,
        LongStringPreference
):
    section = general
    name = 'site_description'
    default = ''
    required = False


@global_preferences_registry.register
class MaintenanceMode(
        preferences.DefaultFromSettingMixin,
        BooleanPreference
):
    """
    Is the instance in maintenance ?
    """
    section = general
    name = 'maintenance_mode'
    default = False


@global_preferences_registry.register
class RegistrationAllowed(
        preferences.DefaultFromSettingMixin,
        BooleanPreference
):
    """
    Are new registrations allowed ?
    """
    verbose_name = 'Allow new users to register'
    section = general
    name = "registration_allowed"
    default = False
