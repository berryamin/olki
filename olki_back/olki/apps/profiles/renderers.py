from olki.apps.core.renderers import OlkiJSONRenderer


class ProfileJSONRenderer(OlkiJSONRenderer):
    object_label = 'profile'
    pagination_object_label = 'profiles'
    pagination_count_label = 'profilesCount'
