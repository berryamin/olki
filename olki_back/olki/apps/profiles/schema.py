from graphene import relay, ObjectType, Field
from graphene_django.types import DjangoObjectType
from graphene_django.filter.fields import DjangoFilterConnectionField

from .models import Profile as ProfileModel


class Profile(DjangoObjectType):
    """
    Profile Node
    """
    class Meta:
        model = ProfileModel
        filter_fields = {
            'bio': ['exact', 'icontains', 'istartswith'],
            }
        interfaces = (relay.Node, )


class ProfileQuery(object):
    """
    what is an abstract type?
    http://docs.graphene-python.org/en/latest/types/abstracttypes/
    """
    profile = relay.Node.Field(Profile)
    profiles = DjangoFilterConnectionField(Profile)


class Viewer(ObjectType):
    profile = Field(Profile)

    @staticmethod
    def resolve_profile(info, **kwargs):
        return info.context.user
