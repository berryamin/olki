from django import forms
import graphene
from graphql_jwt.decorators import login_required
from graphene_django.types import DjangoObjectType
from graphene_django.forms.mutation import ErrorType

from .models import Profile as ProfileModel
from olki.apps.core.graphql.upload.scalars import Upload
from olki.apps.core.graphql.upload.django.mutation import DjangoUploadModelFormMutation


class ProfileForm(forms.ModelForm):
    class Meta:
        model = ProfileModel
        fields = ('name', 'bio', 'indexed', 'private',)
        exclude = ('id',)


class ProfileType(DjangoObjectType):
    class Meta:
        model = ProfileModel
        description = "Type definition for a single profile object"


class ProfileMutation(DjangoUploadModelFormMutation):
    class Input:
        image = Upload()

    class Meta:
        form_class = ProfileForm

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        input['id'] = info.context.user.profile.id

        form = cls.get_form(root, info, **input)
        files = info.context.FILES

        if form.is_valid():
            mutation = cls.perform_mutate(form, info)
            if files:
                print(files)
                print(files['1'])
                info.context.user.profile.image.save(
                    files['1'].name,
                    files['1']
                )
            return mutation
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class ProfileMutations(graphene.AbstractType):
    editProfile = ProfileMutation.Field()
    pass
