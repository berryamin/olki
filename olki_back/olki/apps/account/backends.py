import logging
from django_auth_ldap.backend import LDAPBackend
from django.contrib.auth import get_user_model, get_backends
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q

from .models import LocalityBackendChoice


class OlkiLDAPBackend(LDAPBackend):
    """ A custom LDAP authentication backend """

    def authenticate(self, request, email=None, password=None, **kwargs):
        """ Overrides LDAPBackend.authenticate to save user password in django """
        user_model = get_user_model()
        logging.info("AUTHENTICATING WITH LDAP")

        # email should be the username in ldap (uid)
        try:
            user = LDAPBackend.authenticate(self, request, username=email, password=password)
            # If user has successfully logged, save his (hashed) password locally
            if user:
                user.set_password(password)
                user.save()

            return user
        except Exception as e:
            logging.error(e)
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            user_model().set_password(password)

    def get_or_build_user(self, email, ldap_user):
        """ Overrides LDAPBackend.get_or_create_user to force from_ldap to True """
        kwargs = {
            'email': email,
            'defaults': {
                'from_ldap': True
            }
        }
        user_model = get_user_model()
        return user_model.objects.get_or_create(**kwargs)


class OlkiAuthBackend(ModelBackend):
    """ A custom authentication backend overriding django ModelBackend """

    @staticmethod
    def _is_ldap_backend_activated():
        """ Returns True if OlkiLDAPBackend is activated """
        return OlkiLDAPBackend in [b.__class__ for b in get_backends()]

    def authenticate(self, request, email=None, password=None, **kwargs):
        """ Overrides ModelBackend to refuse LDAP users if OlkiLDAPBackend is activated """
        user_model = get_user_model()

        if self._is_ldap_backend_activated():
            try:
                user_model.objects.get(email=email, locality=LocalityBackendChoice.local)
            except user_model.DoesNotExist:
                return None

        #user = ModelBackend.authenticate(self, email, password, **kwargs)
        try:
            user = user_model.objects.get(Q(actor__username__iexact=email) | Q(email__iexact=email))
            # user = user_model._default_manager.get_by_natural_key(email)
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
            else:
                return None
        except user_model.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            user_model().set_password(password)

        return None
