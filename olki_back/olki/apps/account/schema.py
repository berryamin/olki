from graphene import relay, ObjectType, Field, Boolean, String
from graphene_django.types import DjangoObjectType
from graphene_django.filter.fields import DjangoFilterConnectionField

from .models import User as UserModel, Actor as ActorModel
from olki.apps.profiles.models import Profile as ProfileModel


class Profile(DjangoObjectType):
    class Meta:
        model = ProfileModel
        description = "This is a Profile"
        interfaces = (relay.Node,)

    @classmethod
    def get_node(cls, id, info):
        try:
            profile = cls._meta.model.objects.get(id=id)
        except cls._meta.model.DoesNotExist:
            return None

        if not profile.private:
            return profile
        return None


class Actor(DjangoObjectType):
    class Meta:
        model = ActorModel
        description = "This is an Actor"
        interfaces = (relay.Node,)

    @classmethod
    def get_node(cls, id, info):
        try:
            actor = cls._meta.model.objects.get(id=id)
        except cls._meta.model.DoesNotExist:
            return None

        return actor


class User(DjangoObjectType):
    """
    User Node
    """
    class Meta:
        model = UserModel
        filter_fields = {
            'email': ['exact'],
            'profile__name': ['exact', 'icontains', 'istartswith'],
            'actor__username': ['exact', 'icontains', 'istartswith']
        }
        exclude_fields = ('password', 'is_superuser', 'locality', 'is_inactive_reason')
        description = "This is a User"
        interfaces = (relay.Node, )

    profile = Field(Profile)
    actor = Field(Actor)
    is_setup = Boolean()


class UserQuery(object):
    """
    what is an abstract type?
    http://docs.graphene-python.org/en/latest/types/abstracttypes/
    """
    user = relay.Node.Field(User)
    users = DjangoFilterConnectionField(User)


class ActorQuery(ObjectType):
    has_actor = Field(Boolean, username=String(required=True))

    def resolve_has_actor(self, info, username):
        return ActorModel.objects.filter(username=username).exists()


class Viewer(ObjectType):
    user = Field(User, )

    def resolve_user(self, info, **kwargs):
        if info.context.user.is_authenticated:
            return info.context.user
        return None

    # instance = None  # a lazily-initialized singleton for get_node()
    #
    # @classmethod
    # def get_node(cls, info, id):
    #     if cls.instance is None:
    #         cls.instance = Viewer()
    #     return cls.instance
