from django.core.management.base import BaseCommand

from olki.apps.account.models import User as UserModel


class Command(BaseCommand):
    help = 'Lists local user accounts.'

    def add_arguments(self, parser):
        # Optional argument
        pass

    def handle(self, *args, **options):
        for user in UserModel.objects.all():
            print(user, user.is_active)
