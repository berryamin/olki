from enum import Enum
from django.contrib.auth.models import AbstractUser, BaseUserManager, models
from django.utils.translation import ugettext_lazy as _
from multiselectfield import MultiSelectField
from flax_id.django.fields import FlaxId


class AddFlagOneToOneField(models.OneToOneField):
    def __init__(self, *args, **kwargs):
        self.flag_name = kwargs.pop('flag_name')
        super(AddFlagOneToOneField, self).__init__(*args, **kwargs)

    def contribute_to_related_class(self, cls, related):
        super(AddFlagOneToOneField, self).contribute_to_related_class(cls, related)

        def flag(model_instance):
            return hasattr(model_instance, related.get_accessor_name())
        setattr(cls, self.flag_name, property(flag))

    def deconstruct(self):
        name, path, args, kwargs = super(AddFlagOneToOneField, self).deconstruct()
        kwargs['flag_name'] = self.flag_name
        return name, path, args, kwargs


class InactiveReasonChoice(Enum):
    pendingApproval = "pending approval"
    pendingSelfVerification = "pending self-verification"
    selfDisabled = "self-disabled"
    quarantined = "quarantined"
    banned = "banned"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class LocalityBackendChoice(Enum):
    local = "local"
    ldap = "ldap"
    saml = "saml"  # not yet supported
    cas = "cas"  # not yet supported

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, actorname, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_actor(actorname)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)

        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    # Flax ID field
    # Generates unambiguous, unique distributed ids, with a canonical representation Base64-encoded,
    # URL safe string (16 characters).
    #
    # An ID has 96 bits, of which first 40 bits are the timestamp, and the next 56 bits are random.
    # This gives 2^56 possible unique ids per millisecond.
    #
    # The number 96 was chosen on the following grounds:
    #
    #    It is divisible by 6, so can be rendered as base64-encoded string without padding
    #    It is divisible by 8, so it can be represented as a byte array
    #    It leaves enough room for the random part
    #
    # See http://yellerapp.com/posts/2015-02-09-flake-ids.html for a more thorough explanation.
    #
    # Based on `flax-id`: https://github.com/ergeon/python-flax-id
    id = FlaxId(primary_key=True)

    locality = models.CharField(
        _('locality'),
        editable=False,
        choices=LocalityBackendChoice.choices(),
        default=LocalityBackendChoice.local,
        max_length=5,
        help_text=_(
            'Designates whether a user is local or coming from LDAP,'
            'CAS, SAML, or any other authorization backend.'
        )
    )
    is_inactive_reason = MultiSelectField(
        _('inactive reason'),
        default=None,
        choices=InactiveReasonChoice.choices(),
        help_text=_(
            'Designates the reason(s) a user account is inactive'
            'Some reasons are naturally occuring on account creation,'
            'some are decided by the moderation - they are then cumulative.'
        ),
    )
    email = models.EmailField(
        _('email address'),
        unique=True
    )

    USERNAME_FIELD = 'email'  # extremely important to note. transforms authenticate(username=… in authenticate(email=email, password=password)
    REQUIRED_FIELDS = []

    objects = UserManager()

    @property
    def is_setup(self):
        return self.has_actor

    def set_actor(self, actorname):
        return Actor.objects.create(user=self, username=actorname)

    def __str__(self):
        return self.email


User._meta.get_field('email')._unique = True
User._meta.get_field('username')._unique = False


class Actor(models.Model):
    user = AddFlagOneToOneField(
        User,
        related_name='actor',
        flag_name='has_actor',
        null=True,
        on_delete=models.CASCADE
    )
    username = models.CharField(
        _('actor username'),
        unique=True,
        editable=False,
        max_length=30,
        help_text = _(
            'A unique reference to the user'
            'Required to support the handle username@domain.'
        )
    )
