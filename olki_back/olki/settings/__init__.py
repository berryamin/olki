from pathlib import Path
from split_settings.tools import optional, include

from .environment import PROJECT_DIR


include(
    'components/*.py',
    optional(Path(PROJECT_DIR, 'backend_settings.py')),

    scope=locals()
)
