"""
WSGI config for olki project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import pathlib
import dotenv
from django.core.wsgi import get_wsgi_application

env_path = dotenv.find_dotenv()
os.environ['DOT_ENV_DIRECTORY'] = str(pathlib.Path(env_path).parent)
dotenv.load_dotenv(dotenv_path=env_path)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'olki.settings')

application = get_wsgi_application()
