import graphene
from graphene_django.debug import DjangoDebug
from graphql_jwt.decorators import login_required

from .apps.account.schema import UserQuery, Viewer, ActorQuery
from .apps.account.mutations import UserMutations
from .apps.profiles.mutations import ProfileMutations


class Queries(UserQuery, ActorQuery, graphene.ObjectType):
    """Queries wrapper
    This class inherits from multiple Queries
    as we add more apps queries to our project

    Note it is an instance (graphene.ObjectType)
    inheriting from interfaces (graphene.AbstractType)
    """
    # debug = graphene.Field(DjangoDebug, name='__debug')
    viewer = graphene.Field(Viewer)

    @login_required
    def resolve_viewer(self, info, **kwargs):
        """
        The viewer field represents the currently logged-in user.
        Its subfields expose data that are contextual to the user.
        """
        return info.context.user


class Mutations(UserMutations, ProfileMutations, graphene.ObjectType):
    """Mutations wrapper
    This class inherits from multiple Mutations
    as we add more apps mutations to our project

    Note it is an instance (graphene.ObjectType)
    inheriting from interfaces (graphene.AbstractType)
    """
    pass


schema = graphene.Schema(query=Queries, mutation=Mutations)
