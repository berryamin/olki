#!/bin/sh

# Shell commands follow
# Next line is bilingual: it starts a comment in Python, and is a no-op in shell
""":"

# Find a suitable python interpreter
for cmd in "$(poetry env info -p)/bin/python" python3.7 python3 ; do
   command -v > /dev/null $cmd && exec $cmd $0 "$@"
done

echo "Python interpreter not found" >2

exit 2

":"""
# Previous line is bilingual: it ends a comment in Python, and is a no-op in shell
# Shell commands end here

"""Main entrypoint for the Django app"""

import os
import sys
import pathlib
import dotenv

if __name__ == '__main__':
    env_path = dotenv.find_dotenv()
    os.environ['DOT_ENV_DIRECTORY'] = str(pathlib.Path(env_path).parent)
    dotenv.load_dotenv(dotenv_path=env_path)

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'olki.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
