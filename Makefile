SERVICES = redis postgresql
SHELL := /bin/bash
PATH := $(HOME)/.poetry/bin:$(PATH)

# thanks https://stackoverflow.com/a/47008498
args = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

# thanks https://gist.github.com/prwhite/8168133#gistcomment-2749866
.PHONY: help
help:
	@bash support/makefile/banner

	@awk '{ \
			if ($$0 ~ /^.PHONY: [a-zA-Z\-\_0-9]+$$/) { \
				helpCommand = substr($$0, index($$0, ":") + 2); \
				if (helpMessage) { \
					printf "\033[36m%-20s\033[0m %s\n", \
						helpCommand, helpMessage; \
					helpMessage = ""; \
				} \
			} else if ($$0 ~ /^[a-zA-Z\-\_0-9.]+:/) { \
				helpCommand = substr($$0, 0, index($$0, ":")); \
				if (helpMessage) { \
					printf "\033[36m%-20s\033[0m %s\n", \
						helpCommand, helpMessage; \
					helpMessage = ""; \
				} \
			} else if ($$0 ~ /^##/) { \
				if (helpMessage) { \
					helpMessage = helpMessage"\n                     "substr($$0, 3); \
				} else { \
					helpMessage = substr($$0, 3); \
				} \
			} else { \
				if (helpMessage) { \
					print "\n                     "helpMessage"\n" \
				} \
				helpMessage = ""; \
			} \
		}' \
		$(MAKEFILE_LIST)

## -- Development tasks --

## Launch development environment
serve-dev: check-services deps-dev
	@bash support/makefile/utils ">> Starting frontend at http://localhost:3000 with HMR\n  >> Starting backend at http://localhost:5000"
	@poetry run circusd support/circus.dev.ini || killall olki_front

serve-dev-fe:
	@bash support/makefile/utils ">> Starting frontend at http://localhost:3000 with HMR"
	@cd olki_front ; npm run dev

serve-dev-be: check-services
	@bash support/makefile/utils ">> Starting backend at http://localhost:5000"
	@$(MAKE) manage.py runserver 5000

## Check if required services are running
check-services:
	@bash support/makefile/utils ">> Checking if needed services are running"
	@$(foreach service,$(SERVICES),support/makefile/test-service.sh $(service) || exit;)

## Access Django's manage.py
manage.py:
	@cd olki_back ; ./manage.py $(call args,' --help')

shell-debug:
	@$(MAKE) manage.py shell_plus DEBUG=1

## Launch Django's test suite
test-be:
	@cd olki_back ; poetry run tox

## Launch test suite for the front-end
test-fe:
	@cd olki_front ; npm run test

## -- Maintenance tasks --

## Update the source to the latest stable release (doesn't update the dependencies)
update-release:
	@git fetch
	@git checkout $(git tag -l | sort -Vr | head -n 1)

## Commits a tag, generates changelog, signs packages and uploads a full release
release:
	@command -v release-it >/dev/null 2>&1 || npm install -g release-it # if we lack release-it, we can install it
	@release-it --config support/makefile/.release-it.json

generate-setuppy-reqs:
	@poetry run poetry-setup

## Install development dependencies (production + extra depedencies)
deps-dev:
	@bash support/makefile/utils ">> Checking if needed dependencies are installed"
	@$(MAKE) deps-dev-be
	@$(MAKE) deps-dev-fe

deps-dev-be:
	@command -v poetry >/dev/null 2>&1 || curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python # if we lack poetry, we can install it
ifndef CI
	@cd olki_back ; poetry install --no-interaction # backend dependencies (python)
	@poetry run therapist install
else
	@cd olki_back ; poetry install -vvv --no-interaction # backend dependencies (python)
endif

deps-dev-fe:
	@command -v node >/dev/null 2>&1 || exit 1 # if we lack node, we can only fail since it is not our responsibility to install it
	@cd olki_front ; npm i # frontend dependencies (javascript)

## Install production dependencies (production-only dependencies)
deps-prod:
	@bash support/makefile/utils ">> Checking if needed production dependencies are installed"
	@$(MAKE) deps-prod-be
	@$(MAKE) deps-prod-fe

deps-prod-be:
	@command -v poetry >/dev/null 2>&1 || curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python # if we lack poetry, we can install it
	@cd olki_back ; poetry install --no-interaction --no-dev # backend dependencies (python)

deps-prod-fe:
	@command -v node >/dev/null 2>&1 || exit 1 # if we lack node, we can only fail since it is not our responsibility to install it
	@cd olki_front ; npm i --production # frontend dependencies (javascript)

##-- Production tasks --

## Launch production environment (EXPERIMENTAL)
serve-prod: check-services deps-prod
	@bash support/makefile/utils ">> Starting frontend at http://localhost:5000\n  >> Starting backend at http://localhost:5005"
	@poetry run circusd support/circus.prod.ini

build-fe:
	@bash support/makefile/utils ">> Building OLKi FE"
	@cd olki_front ; npm run build

%:
	@:
